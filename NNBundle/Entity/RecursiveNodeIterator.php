<?php

namespace DotaUnderground\NNBundle\Entity;

use Doctrine\Common\Collections\Collection;
use RecursiveIterator;

/**
 * Class RecursiveNodeIterator
 * @package DotaUnderground\NNBundle\Entity
 */
class RecursiveNodeIterator implements \RecursiveIterator
{
    /**
     * @var Collection
     */
    private $data;

    public function __construct(Collection $data)
    {
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return $this->data->current();
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        return $this->data->next();
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return $this->data->key();
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return $this->data->current() instanceof Node;
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        $this->data->first();
    }

    /**
     * {@inheritdoc}
     */
    public function hasChildren()
    {
        return (!$this->data->current()->getChildNodes()->isEmpty());
    }

    /**
     * {@inheritdoc}
     */
    public function getChildren()
    {
        return new RecursiveNodeIterator($this->data->current()->getChildNodes());
    }

}
