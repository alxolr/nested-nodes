<?php
namespace DotaUnderground\NNBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Node
 * @ORM\Entity()
 * @ORM\Table(name="node")
 */
class Node
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=4, nullable=false)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Node", inversedBy="childNodes")
     * @ORM\JoinColumn(name="parent_node_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $parentNode;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Node", mappedBy="parentNode")
     */
    protected $childNodes;

    public function __construct()
    {
        $this->childNodes = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getChildNodes()
    {
        return $this->childNodes;
    }

    /**
     * @param ArrayCollection $childNodes
     */
    public function setChildNodes($childNodes)
    {
        $this->childNodes = $childNodes;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getParentNode()
    {
        return $this->parentNode;
    }

    /**
     * @param mixed $parentNode
     */
    public function setParentNode($parentNode)
    {
        $this->parentNode = $parentNode;
    }
}
