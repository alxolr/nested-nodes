<?php
namespace DotaUnderground\NNBundle\Twig;

use DotaUnderground\NNBundle\Entity\Node;

/**
 * Class TreeExtension
 * @package DotaUnderground\NNBundle\Twig
 */
class TreeExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction(
                'treeify', array($this, 'treeify', array('is_safe' => array('html')))
            )
        );
    }

    /**
     * @param \RecursiveIteratorIterator $iterator
     * @return string
     */
    public function treeify(\RecursiveIteratorIterator $iterator)
    {
        $result = "<section>";
        foreach ($iterator as $key => $childNode) {
            /** @var Node $childNode */

            $html = "<div>" . str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $iterator->getDepth()) .
                "{$childNode->getName()}
                <a href='/info/nested-nodes/add/{$childNode->getId()}'><i
                 class='fa fa-plus text-success'></i></a>
                 <a href='/info/nested-nodes/remove/{$childNode->getId()}'><i class='fa fa-minus text-danger'></i></a>
                </div>
                <hr>";
            $result .= $html;
        }

        $result .= "</section>";

        return $result;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'tree';
    }

}
