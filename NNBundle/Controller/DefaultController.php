<?php

namespace DotaUnderground\NNBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use DotaUnderground\NNBundle\Entity\Node;
use DotaUnderground\NNBundle\Entity\RecursiveNodeIterator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/info/nested-nodes")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $recursiveIterator = $this->getRecursiveIterator($em);

        return array(
            'iterator' => $recursiveIterator
        );
    }

    /**
     * @Route("/info/nested-nodes/remove/{node}", name="remove_node")
     * @Template("DotaUndergroundNNBundle:Default:index.html.twig")
     */
    public function removeNodeAction(Node $node)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($node);
        $em->flush();

        $recursiveIterator = $this->getRecursiveIterator($em);

        return array(
            'iterator' => $recursiveIterator
        );
    }

    /**
     * @Route("/info/nested-nodes/add/{node}", name="add_node")
     * @Template("DotaUndergroundNNBundle:Default:index.html.twig")
     */
    public function addNodeAction(Node $node)
    {
        $em = $this->getDoctrine()->getManager();
        $newNode = new Node();
        $newNode->setParentNode($node);
        $newNode->setName('node');

        $em->persist($newNode);
        $em->flush();

        $recursiveIterator = $this->getRecursiveIterator($em);

        return array(
            'iterator' => $recursiveIterator
        );
    }

    /**
     * @param $em
     * @return \RecursiveIteratorIterator
     */
    private function getRecursiveIterator($em)
    {
        $rootNode = $em->getRepository('DotaUndergroundNNBundle:Node')->findBy(
            array(
                'parentNode' => null
            )
        );

        $collection = new ArrayCollection($rootNode);
        $nodeIterator = new RecursiveNodeIterator($collection);
        $recursiveIterator = new \RecursiveIteratorIterator($nodeIterator, \RecursiveIteratorIterator::SELF_FIRST);

        return $recursiveIterator;
    }
}
